// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VLSUserWidget.h"
#include "VLSEditTeamWidget.generated.h"

/**
 * 
 */
UCLASS()
class VIRTUALLIVESOCCER_API UVLSEditTeamWidget : public UVLSUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
		void InitUI(class AVirtualLiveSoccerGameMode *gameMode);

	UFUNCTION()
		void FillUI();

	UPROPERTY(meta = (BindWidget))
		class UButton* CancelButton;

protected:

	class AVirtualLiveSoccerGameMode *_gameMode;


	
	
};
